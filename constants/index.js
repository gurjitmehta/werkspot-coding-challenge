export const BASE_API_URL = 'https://hacker-news.firebaseio.com/v0/';
export const MAX_STORIES_TO_LOAD = 20;