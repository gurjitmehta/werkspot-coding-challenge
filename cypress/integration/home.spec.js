const maxStoriesToLoadAtTime = Cypress.env("MAX_STORIES_TO_LOAD");
const baseApiUrl = Cypress.env("BASE_API_URL");

describe('Homepage', () => {
  const loadStoriesButton = 'loadTopStories';

  beforeEach(() => {
    cy.visit('http://localhost:1234');
  });


  context('initial load', () => {
    it('successfully loads', () => {
      cy.visit('http://localhost:1234')
    })

    it(`should change the ${loadStoriesButton} button text appropriately`, () => {
      const btnText = `Load ${maxStoriesToLoadAtTime} top-stories`;
      cy.get('#load-top-stories').should('have.text', btnText);
    })
  })


  context('load top stories', () => {
    it(`should load ${maxStoriesToLoadAtTime} stories when clicked on ${loadStoriesButton} button`, () => {
      cy.server();

      cy.get('button#load-top-stories').as('loadTopStoriesControl');

      cy.get('@loadTopStoriesControl')
        .wait(500) //TODO: remove wait by some plugin
        .click()

      cy.get('.story').should('have.length', maxStoriesToLoadAtTime);
    })
  })

  context('load-more-stories button', () => {
    it(`should show load-more-stories button after loading ${maxStoriesToLoadAtTime} stories`, () => {
      cy.server();

      cy.get('#load-top-stories').as('loadTopStoriesControl');

      cy.get('@loadTopStoriesControl')
        .wait(500) //TODO: remove wait by some plugin
        .click()

      cy.get('#load-more-stories')
        .should('be.visible')
        .should('have.class', 'btn')
        .should('have.class', 'btn-primary')
    })

    it(`load-more-stories button should load ${maxStoriesToLoadAtTime} more stories`, () => {
      cy.server();

      cy.get('#load-top-stories').as('loadTopStoriesControl');

      cy.get('@loadTopStoriesControl')
        .wait(500) //TODO: remove wait by some plugin
        .click()


      cy.get('.story').should('have.length', maxStoriesToLoadAtTime)

      cy.get('#load-more-stories')
        .click()

      cy.get('.story').should('have.length', maxStoriesToLoadAtTime + maxStoriesToLoadAtTime);
    })
  })

})