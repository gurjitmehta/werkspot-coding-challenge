import { getStoryDom } from '../ui/get-story.dom';
import { showStories } from '../ui/show-stories.dom';

import { fetchStoriesIds } from '../utils/fetch-stories-ids';
import { fetchStories } from '../utils/fetch-stories';
import { MAX_STORIES_TO_LOAD } from '../constants';

import globalState from '../state/globalstate';

const loadStories = async function () {

  const mainContainer = document.getElementById('app');
  const btnText = this.innerHTML;
  this.setAttribute('disabled', true);
  this.innerHTML = 'LOADING.....';

  try {
    const stories = await fetchStories();
    const storiesDom = stories.map(story => getStoryDom({ story }));
    showStories({ stories: storiesDom });
  } catch (err) {
    const topStoriesState = globalState.getState('topStories');
    topStoriesState.lastFetchedId = topStoriesState.lastFetchedId - MAX_STORIES_TO_LOAD;
    globalState.setState('topStories', topStoriesState);
  } finally {

    const loadMoreStoriesBtn = document.getElementById('load-more-stories');
    if (loadMoreStoriesBtn) {
      loadMoreStoriesBtn.remove();
    }

    // create a new loadStories button
    const newLoadMoreStoriesBtn = document.createElement('button');
    newLoadMoreStoriesBtn.setAttribute('id', 'load-more-stories');
    newLoadMoreStoriesBtn.classList.add('btn');
    newLoadMoreStoriesBtn.classList.add('btn-primary');
    newLoadMoreStoriesBtn.style.margin = '8px auto';
    newLoadMoreStoriesBtn.innerHTML = 'Load more stories';
    newLoadMoreStoriesBtn.addEventListener('click', loadStories);

    mainContainer.appendChild(newLoadMoreStoriesBtn);
  }
  this.removeAttribute('disabled');
  this.innerHTML = btnText;
}

export const initDom = async () => {

  const loadTopStoriesBtn = document.getElementById('load-top-stories');
  loadTopStoriesBtn.innerHTML = `Load ${MAX_STORIES_TO_LOAD} top-stories`;

  // PreFetch all the stories ids.
  const topStoriesState = globalState.getState('topStories');
  const ids = await fetchStoriesIds();
  topStoriesState.ids = ids;
  globalState.setState('topStories', topStoriesState);

  // Load & Show desired no of stories
  loadTopStoriesBtn.addEventListener('click', loadStories);
}