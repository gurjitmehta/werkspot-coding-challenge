// @stories will be array [story1DomNode, story2DomNOde....]
export const showStories = ({ stories, storiesType = 'top-stories' }) => {
  const storiesWrapper = document.getElementById(storiesType);

  stories.forEach(storyDomNode => {
    storiesWrapper.appendChild(storyDomNode);
  });
}