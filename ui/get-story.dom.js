export const getStoryDom = ({ story }) => {

  const storyWrapper = document.createElement('div');
  storyWrapper.setAttribute('id', story.id);
  storyWrapper.setAttribute('class', 'story');

  const storyTitle = document.createElement('h2')
  storyTitle.setAttribute('class', 'story--title');
  storyTitle.innerHTML = story.title;

  const writtenByText = document.createElement('p');
  writtenByText.innerHTML = 'written by';
  writtenByText.style.marginBottom = '4px';
  writtenByText.style.opacity = '0.6';

  const storyAuthor = document.createElement('p')
  storyAuthor.setAttribute('class', 'story--author');
  storyAuthor.innerHTML = story.by;

  const storyUrl = document.createElement('a')
  storyUrl.setAttribute('href', story.url);
  storyUrl.setAttribute('target', '_blank');
  storyUrl.setAttribute('class', 'story--url');
  storyUrl.innerHTML = 'story url';

  // Append all the children
  storyWrapper.appendChild(storyTitle);
  storyWrapper.appendChild(writtenByText);
  storyWrapper.appendChild(storyAuthor);
  storyWrapper.appendChild(storyUrl);
  return storyWrapper
}
