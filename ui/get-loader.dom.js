export const getLoader = () => {
  const loader = document.createElement('div');
  loader.setAttribute('id', 'loader');
  return loader;
}