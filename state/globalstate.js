const stories = {
  ids: [],
  lastFetchedId: 0,
}


const initialState = {
  isLoading: false,
  topStories: stories,
}

class GlobalState {
  constructor({ isLoading, topStories }) {
    this.isLoading = isLoading;
    this.topStories = topStories;
  }

  getState(stateProperty) {
    return Object.assign({}, this[stateProperty]);
  }

  setState(stateProperty, data) {
    this[stateProperty] = data;
  }
}

export default new GlobalState(initialState);