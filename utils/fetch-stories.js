import { fetchStoryItem } from './fetch-story-item';
import { MAX_STORIES_TO_LOAD } from '../constants';
import globalState from '../state/globalstate';
export const fetchStories = async (storyType = 'topStories') => {
  const topStories = globalState.getState('topStories');
  const ids = topStories.ids;
  const lastFetchedId = topStories.lastFetchedId;
  const startFetchingFrom = lastFetchedId;
  const endFetchingOn = Math.min(startFetchingFrom + MAX_STORIES_TO_LOAD, ids.length);
  const storiesIdsToFetch = ids.slice(startFetchingFrom, endFetchingOn);
  const storiesPromises = [];

  for (let i = 0; i < storiesIdsToFetch.length; ++i) {
    storiesPromises.push(fetchStoryItem(storiesIdsToFetch[i]));
  }

  topStories.lastFetchedId = endFetchingOn;
  globalState.setState('topStories', topStories);

  return await Promise.all(storiesPromises);
}