import { BASE_API_URL } from '../constants';

export const buildApiUrl = (url) => {
  return BASE_API_URL + url;
}