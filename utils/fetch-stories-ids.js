import globalState from '../state/globalstate';
import { buildApiUrl } from './build-api-url';
export const fetchStoriesIds = async (storiesType = "top") => {
  const ids = globalState[`${storiesType}Stories`].ids;
  if (ids.length > 0) {
    return new Promise.resolve(ids);
  }
  const apiUrl = buildApiUrl(`${storiesType}stories.json`);
  const storiesResponse = await fetch(apiUrl);
  return storiesResponse.json();
}