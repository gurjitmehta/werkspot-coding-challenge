import { buildApiUrl } from './build-api-url';
export const fetchStoryItem = async (storyItemId) => {
  const response = await fetch(buildApiUrl(`/item/${storyItemId}.json`));

  return response.json();
}